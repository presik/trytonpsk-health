# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import fields, ModelSQL, ModelView
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Configuration(ModelSQL, ModelView):
    'Health Configuration'
    __name__ = 'health.configuration'
    company = fields.Many2One('company.company', 'Company', required=True)
    patient_sequence = fields.Many2One('ir.sequence', 'Patient Sequence',
        required=True)
    patient_evaluation_sequence = fields.Many2One(
        'ir.sequence', 'Patient Evaluation Sequence', required=True)
    appointment_sequence = fields.Many2One(
        'ir.sequence', 'Appointment Sequence', required=True)
    prescription_sequence = fields.Many2One(
        'ir.sequence', 'Prescription Sequence', required=True)

    @classmethod
    def get_config(cls):
        configs = cls.search([
            ('company', '=', Transaction().context.get('company'))
        ])
        if not configs:
            raise UserError(gettext('health.msg_missing_config_health'))
        return configs[0]
