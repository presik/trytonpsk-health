
from trytond.wizard import Wizard, StateView, Button, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction


class CheckImmunizationStatusInit(ModelView):
    'Check Immunization Status Init'
    __name__ = 'health.check_immunization_status.init'
    immunization_schedule = \
        fields.Many2One("health.immunization_schedule", "Schedule",
        required=True)


class CheckImmunizationStatus(Wizard):
    'Check Immunization Status'
    __name__ = 'health.check_immunization_status'

    start = StateView('health.check_immunization_status.init',
        'health.view_check_immunization_status', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Immunization Status', 'check_immunization_status',
                'tryton-ok', True),
            ])
    check_immunization_status = StateAction('health.report_immunization_status')

    def do_check_immunization_status(self, action):
        return action, self.get_info()

    def get_info(self):

        return {
            'patient_id': Transaction().context.get('active_id'),
            'immunization_schedule_id': self.start.immunization_schedule.id
            }

    def transition_check_immunization_status(self):
        return 'end'
