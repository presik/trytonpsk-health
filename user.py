# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pool import Pool
from trytond.transaction import Transaction

class User(metaclass=PoolMeta):
    __name__ = 'res.user'
    healthprof = fields.Many2One('health.professional', 'Health Professional')
    user = fields.Many2One('res.user', 'User')

    @classmethod
    def get_user(cls):
        # Get the user.
        User = Pool().get('res.user')
        user = User(Transaction().user)

        return user.id
